/******
This page contains 3 versions of the Node class. You should
comment / uncomment the version you are interested to use.
******/

/*
// Basic node class using integers
public class Node {
	public static void main(String[] args) {
		Node x = new Node(1, null);
		Node y = new Node(2, x);
		System.out.println( y.getData() );
		System.out.println( y.getNext().getData() );
	}
	
	private int data;
	private Node next;
	
	public Node(int d, Node nx) {
		data = d;
		next = nx;
	}
	
	public int getData() { return data; }
	public Node getNext() { return next; }
	public void setData(int d) { data = d; }
	public void setNext(Node n) { next = n; }
} 
*/

/* 
// Node class with Generics
public class Node<T> {
	public static void main(String[] args) {
		Node<String> x = new Node<String>("Hello", null);
		Node<String> y = new Node<String>("World", x);
		System.out.println( y.getData() );
		System.out.println( y.getNext().getData() );
	}
	
	private T data;
	private Node next;
	
	public Node(T d, Node nx) {
		data = d;
		next = nx;
	}
	
	public T getData() { return data; }
	public Node getNext() { return next; }
	public void setData(T d) { data = d; }
	public void setNext(Node n) { next = n; }
} */



// Node class with generics and arbitrary number of connections
import java.util.ArrayList;

public class Node<T> {
	private T data;
	private ArrayList<Node> neighbors;
	
	public Node(T d) {
		data = d;
		neighbors = new ArrayList<Node>();
	}
	
	public T getData() { return data; }
	public void setData(T d) { data = d; }
	
	public Node[] getNeighbors() {
		return neighbors.toArray(new Node[0]);
	}
	public void setNeighbors(Node[] d) {
		for (int i=0; i<d.length; i++) {
			neighbors.add( d[i] );
		}
	}
}	

